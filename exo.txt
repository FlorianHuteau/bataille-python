exo.txt

Jeu de Bataille

Simuler un jeu de bataille en représentant les cartes de la manière désirée.
Le jeu doit se dérouler avec un ensemble de 52 cartes de 13 valeurs différentes,
du 2 à l'As.

Vous pouvez représenter les cartes soit avec : 
- un simple objet `int` (vous perdez donc le symbole associé, à savoir trèfle etc.)
- un objet d'une classe que vous définissez, avec un attribut "value" et un attribut "symbol" et/ou un attribut "name" (par exemple, le valet de pique se nomme Ogier)

Vous possédez une pioche contenant des cartes disponibles. Cette pioche peut être :
- Un objet d'une classe de votre choix, ce qui permettrait d'écrire des méthodes pour en gérer le contenu
- Une simple liste (ou un ensemble)

Ladite pioche devrait contenir des cartes mélangeables pour démarrer des parties imprévisibles.

Pour servir les cartes des deux joueurs, vous devez 'déplacer' une carte, de la pioche
vers le jeu en main d'un joueur. (copier la carte vers la main et la supprimer de la pioche)


Lorsque le jeu commence, vous pouvez choisir une carte de votre main (on doit donc vous afficher les cartes encore disponibles)
qui sera posée sur la table, et l'adversaire vous y opposera une de ses cartes (au hasard).

Vous gagnez lorsque votre carte a une valeur supérieure à la sienne.

Vous effectuez des tours tant qu'il vous reste des cartes en main.
À la fin, l'ordinateur vous affiche le décompte des batailles que vous avez gagnées et perdues. 