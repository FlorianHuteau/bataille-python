from module.mainCarte import MainCarte
from module.pioche import Pioche
from colorama import Fore

"""
    Partie de bataille entre deux joueurs
    Mise en place de la partie :
    - Création de la pioche
    - Mélange de la pioche
    - Création des mains des deux joueurs
    - Remplissage des mains
    La partie commence :
    - Les deux joueurs jouent chacun une carte
    - On compare les deux cartes :
        - Le joueur qui perds pioche une carte
        - Si égalité :
            - On recommence
            - Si un joueur n'a plus de carte dans sa main, il pioche :
                - S'il n'y a plus de carte dans le pioche, il y a égalité
    - La partie se termine quand un joueur n'a plus de carte et a gagné la dernière bataille
"""

if __name__ == "__main__":

    # Mise en place de la partie :
    #   On créé la pioche
    pioche = Pioche()
    #   On mélange la pioche
    pioche.melanger()

    #   On créé les mains des deux joueurs
    joueur1 = MainCarte()
    joueur2 = MainCarte()

    #   Les deux joueurs piochent remplissent leur main
    for i in range(5):
        joueur1.piocheCarte(pioche)
        joueur2.piocheCarte(pioche)
        # print(f"joueur1: {len(joueur1.main)} joueur2: {len(joueur2.main)}")
        pass

    # La partie commence :
    #   Tant qu'il n'y a pas de vainqueur
    victoire = False
    while not victoire:
        # On affiche la main du joueur 1
        print("Main du " + Fore.YELLOW + "joueur 1 :" + Fore.RESET)
        joueur1.afficherMain()
        # Il choisit la carte qu'il veut jouer
        carte1 = joueur1.jouerCarte()
        # On prends une carte du " + Fore.RED + "joueur 2 :" + Fore.RESET + "
        carte2 = joueur2.jouerCarte(True)
        # On compare les cartes et le joueur perdant pioche une carte, si la pioche n'est pas vide
        bataille = carte1.compare(carte2)
        if bataille == 1:
            if not pioche.estVide():
                print(
                    "Le "
                    + Fore.YELLOW
                    + "joueur 1 :"
                    + Fore.RESET
                    + " pioche une carte"
                )
                joueur1.piocheCarte(pioche)
        elif bataille == -1:
            if not pioche.estVide():
                print(
                    "Le " + Fore.RED + "joueur 2 :" + Fore.RESET + " pioche une carte"
                )
                joueur2.piocheCarte(pioche)

        # On affiche le nombre de carte du joueur 2
        print(
            "Le " + Fore.RED + "joueur 2 :" + Fore.RESET + f" a {len(joueur2)} cartes"
        )

        # On vérifie si un joueur n'a plus de carte
        if joueur1.estVide():
            print("Le " + Fore.YELLOW + "joueur 1 :" + Fore.RESET + " a gagné")
            victoire = True
        elif joueur2.estVide():
            print("Le " + Fore.RED + "joueur 2 :" + Fore.RESET + " a gagné")
            victoire = True

    print("Fin de la partie")
