from typing import Literal

from colorama import Fore


class Carte:
    """
    Classe représentant une carte à jouer

    Elle doit afficher sa valeur et sa couleur
    Elle doit pouvoir être comparée à une autre carte
    """

    def __init__(self, couleur, valeur):
        self.couleur: Literal["coeur ♥", "trefle ♣", "carreau ♦", "pique ♠"] = couleur
        self.valeur: Literal[
            "As", "Roi", "Dame", "Vallet", "10", "9", "8", "7", "6", "5", "4", "3", "2"
        ] = valeur
        self.force: int = {
            "As": 14,
            "Roi": 13,
            "Dame": 12,
            "Vallet": 11,
            "10": 10,
            "9": 9,
            "8": 8,
            "7": 7,
            "6": 6,
            "5": 5,
            "4": 4,
            "3": 3,
            "2": 2,
        }[valeur]

    def __str__(self):
        return f"{self.valeur} de {self.couleur}"

    def compare(self, carte: "Carte") -> int:
        print(
            "Le "
            + Fore.YELLOW
            + "joueur 1 :"
            + Fore.RESET
            + f" joue {self} et le "
            + Fore.YELLOW
            + "joueur 2 :"
            + Fore.RESET
            + f" joue {carte}"
        )
        result = self.force - carte.force
        if result < 0:
            print("Le " + Fore.RED + "joueur 2 :" + Fore.RESET + " gagne la bataille")
            return 1
        elif result > 0:
            print(
                "Le " + Fore.YELLOW + "joueur 1 :" + Fore.RESET + " gagne la bataille"
            )
            return -1
        else:
            print("Egalité")
            return 0
