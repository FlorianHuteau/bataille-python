from .pioche import Pioche
from .carte import Carte


class MainCarte:
    """
    Classe qui représente la main d'un joueur
    Une main a une taille maximale
    Elle doit pouvoir ajouter une carte à la main
    Elle doit pouvoir retirer une carte de la main
    Elle doit pouvoir afficher la main
    """

    def __init__(self):
        self.main = []
        self.tailleMax = 5

    def piocheCarte(self, pioche: Pioche) -> bool:
        if len(self.main) < self.tailleMax:
            carte = pioche.piocher()
            self.main.append(carte)
            return True
        else:
            print("La main est pleine")
            return False

    def jouerCarte(self, ordi=False) -> Carte:
        if ordi:
            return self.main.pop(0)

        while True:
            try:
                index = int(input("Quelle carte voulez-vous jouer ? ")) - 1
                if 0 <= index < len(self.main):
                    break
                else:
                    print("Veuillez choisir une carte dans votre main")
            except ValueError:
                print("Veuillez entrer un nombre entier valide")

        return self.main.pop(index)

    def afficherMain(self) -> None:
        index = 1
        for carte in self.main:
            print(f"{index} : {carte}")
            index += 1

    def estVide(self) -> bool:
        return len(self.main) == 0

    def __len__(self):
        return len(self.main)
