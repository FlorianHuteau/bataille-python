from .carte import Carte
import random


class Pioche:
    """
    Classe Pioche
    Elle doit pouvoir créer une pioche de 52 cartes
    Elle doit pouvoir mélanger la pioche
    On doit pouvoir piocher une carte
    On doit pouvoir verifier si la pioche est vide
    """

    def __init__(self):
        self.pioche: list = []
        for couleur in ["coeur ♥", "trefle ♣", "carreau ♦", "pique ♠"]:
            for valeur in [
                "As",
                "Roi",
                "Dame",
                "Vallet",
                "10",
                "9",
                "8",
                "7",
                "6",
                "5",
                "4",
                "3",
                "2",
            ]:
                self.pioche.append(Carte(couleur, valeur))

        self.taille = len(self.pioche)
        # print("La pioche est créée")

    def melanger(self) -> None:
        random.shuffle(self.pioche)
        # print("La pioche est mélangée")

    def piocher(self) -> Carte:
        self.taille = len(self.pioche) - 1
        return self.pioche.pop()

    def estVide(self) -> bool:
        # On alerte s'il ne reste plus beaucoup de carte
        alerte = [10, 5, 1]
        if self.taille in alerte:
            print(f"Il reste {self.taille} cartes dans la pioche")
            return False
        elif len(self.pioche) == 0:
            print("La pioche est vide")
            return True
        else:
            return False
